const router = require('express').Router()

const CarsController = require('../controllers/Cars')
const VerifyToken = require('../middleware/VerifyToken')
const multipart = require('connect-multiparty')

router.get('/getCars', VerifyToken, CarsController.getCars)

router.post('/getCars', VerifyToken, CarsController.getCars)

router.post('/addCar', VerifyToken, CarsController.createCar)

router.post('/getCar',  VerifyToken, CarsController.getCarOne)

router.post('/updateCar', VerifyToken, CarsController.updateCar)

router.post('/deleteCar', VerifyToken, CarsController.deleteCar)

router.use(multipart({
    uploadDir: 'tmp'
}))

router.post('/uploadphoto', CarsController.uploadPhotos)

module.exports = router