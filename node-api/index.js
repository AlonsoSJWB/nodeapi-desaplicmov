const mongoose = require('mongoose')
const app = require('./app')
const port = process.env.PORT || 3000

const uat = 'mongodb+srv://alonsowb:xTx4Ioz9fnCA1tsX@carsdatabase-axcki.mongodb.net/test?retryWrites=true&w=majority'
const local = 'mongodb://localhost:27017/dbase'

mongoose.connect(local, (err, res)=>{
    if(err){
        return console.log(`Error connecting to the database ${err}`)
    }

    console.log('Connection to the database established')

    app.listen(port, ()=>{
        console.log(`API Rest running at http://localhost:${port}`)
    })
})