const express = require('express')
const bodyParser = require('body-parser')
const cars = require('./routes/cars')
const auth = require('./routes/auth')

const app = express()

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())

app.get('/', (req,res)=>{
    res.send("Hola mundo")
})
app.use('/auth', auth)
app.use('/cars', cars)

module.exports = app