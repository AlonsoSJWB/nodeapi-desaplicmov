const mongoose = require('mongoose')
const meteorID = require('meteor-mongo-id')
const Random = require('meteor-random')
const Schema = mongoose.Schema

const CarsSchema = Schema({
    _id:{ type: String},
    model: { type: String },
    year: { type: String },
    brand: { type: String },
    color: { type: String },
    noSerie: { type: String },
    plate: { type: String },
    version: { type: String }
})

mongoose.Schema.ObjectId.get(v => v != null ? v.toString() : v);

module.exports = mongoose.model('Cars', CarsSchema)