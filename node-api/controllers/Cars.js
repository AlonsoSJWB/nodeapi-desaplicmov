module.exports = {
    getCars,
    getCarOne,
    createCar,
    getCarsPagination,
    updateCar,
    deleteCar,
    uploadPhotos
}

const CarsSub = require('../models/Cars')
const mongoose = require('mongoose')
const meteorID = require('meteor-mongo-id')
const Random = require('meteor-random')
const cloudinary = require('cloudinary').v2
const fs = require('fs')

cloudinary.config({
    cloud_name: 'ravenegg',
    api_key: '173273979277351',
    api_secret: 'zGjYH6vwUSalJPm2sgSevqUMNaM'
})

function getCars(req, res){
    CarsSub.find({}, (err, concepts)=>{
        if(err) return res.status(500).send({message: `Problem with the searching request ${err}`})
        if(!concepts) return res.status(404).send({message:`Cars does not exist`})

        res.status(200).send({cars: concepts})
    })
}

function getCarsPagination(req, res){
    let perPage = parseInt(req.body.perPage)
    let page = parseInt(req.body.page)
    let carsConceptsRes = null;

    let searchData = req.query.search

    CarsSub.find(searchData).skip((page - 1) * perPage)
    .limit(perPage)
    .sort({})
    .exec()
    .then((concepts)=>{
        res.set('X-limit', perPage)
        res.set('X-page', page)
        carsConceptsRes = concepts
        console.info("Result", concepts)
        return CarsSub.count()
    })
    .then((total)=>{
        res.set('X-total', total)
        res.status(200).send({ total: total, carsTotal: carsConceptsRes.length, carsConcepts: carsConceptsRes})
    })
    .catch((err)=>{
        console.log(err);
        res.status(500).send({message: `Error in request ${err}`})
    })

}

function getCarOne(req, res){
    let conceptID = req.body._id
    CarsSub.find({_id: conceptID}, (err, concept)=>{
        if(err) return res.status(500).send({message: `Problem with the searching request ${err}`})
        if(!concept) return res.status(404).send({message:`Car does not exist`})
        console.log()
        res.status(200).send({car: concept})
    })
}

function createCar(req, res){
    let car = new CarsSub(req.body)
    car._id = Random.id()

    car.save((err, carStored)=>{
        if(err) return res.status(400).send({message: `Error on model ${err}`})

        res.status(200).send({car: carStored})
    })
}

function updateCar(req, res){
   let conceptID = req.body._id
   let update = req.body.car
   
   /*CarsSub.findOne({_id: conceptID}, (err, conceptCar)=>{
       if(err) return res.status(500).send({message: `Problem with the searching request ${err}`})
       if(!conceptCar) return res.status(404).send({message: 'The car does not exist'})
   })*/

   CarsSub.findByIdAndUpdate(conceptID, update, (err, concept)=>{
    if(err) return res.status(500).send({message: `Problem with the searching request ${err}`})
    res.status(200).send({message: `Update Successfull`, car: concept})
   })
}

function deleteCar(req, res){
    const conceptID = req.body._id

    CarsSub.remove({_id: conceptID}, (err, concept)=>{
        if(err) return res.status(500).send({message: `Problem with the searching request ${err}`})
        res.status(200).send({message: `Remove Completed`})
    })

}

function uploadPhotos(req, res){
    const path = req.files.file
    console.log(typeof path.path)
    fs.readFile(path.path, function(err, data){
        if(err) { throw err }
        cloudinary.uploader.upload(path.path).then(function(res){
            console.log(res)
        })
    })
    
}